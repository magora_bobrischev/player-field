package android.support.v7.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import com.mgrmoby.ontheteam.lineout.info.LineoutInfo;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public class LineoutLayoutManager extends LinearLayoutManager {
    private LineoutInfo lineoutInfo;

    public LineoutLayoutManager(Context context, LineoutInfo lineoutParams, boolean reverseLayout) {
        super(context, LineoutLayoutManager.VERTICAL, reverseLayout);
        this.lineoutInfo = lineoutParams;
    }

    public void swapInfo(@NonNull LineoutInfo newInfo){
        lineoutInfo = newInfo;
    }

    @Override
    void layoutChunk(RecyclerView.Recycler recycler, RecyclerView.State state, LayoutState layoutState, LayoutChunkResult result) {
        View view = layoutState.next(recycler);
        if (view == null) {
            // if we are laying out views in scrap, this may return null which means there is no more items to layout.
            result.mFinished = true;
            return;
        }
        RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) view.getLayoutParams();
        if (layoutState.mScrapList == null) {
            if (mShouldReverseLayout == (layoutState.mLayoutDirection == LayoutState.LAYOUT_START)) {
                addView(view);
            } else {
                addView(view, 0);
            }
        } else {
            if (mShouldReverseLayout == (layoutState.mLayoutDirection == LayoutState.LAYOUT_START)) {
                addDisappearingView(view);
            } else {
                addDisappearingView(view, 0);
            }
        }
        int left;
        int top;
        int right;
        int bottom;
        int position = getPosition(view);
        int recyclerViewWidth = getWidth();
        int recyclerViewHeight = getHeight() - getPaddingTop() - getPaddingBottom();
        final int rowsCount = lineoutInfo.getRowsCount();
        final int rowIndex = lineoutInfo.rowIndexForPosition(position);
        final int columnIndex = lineoutInfo.columnIndexForPosition(rowIndex, position);
        final int columnsCount = lineoutInfo.columnsInRow(rowIndex);
        final int viewWidth = (recyclerViewWidth - getPaddingLeft() - getPaddingRight()) / lineoutInfo.columnsInRow(rowIndex);
        final int viewHeight = recyclerViewHeight / rowsCount;

        params.width = viewWidth;
        params.height = viewHeight;
        boolean leftMost = columnIndex == 0;
        boolean rightMost = columnIndex == columnsCount - 1;

        if (!leftMost && !rightMost) {
            left = getPaddingLeft() + columnIndex * viewWidth;
            right = left + viewWidth;
        } else {
            if (leftMost) {
                left = getPaddingLeft();
                if (rightMost) {
                    right = recyclerViewWidth - getPaddingRight();
                } else {
                    right = left + viewWidth;
                }
            } else {
                right = recyclerViewWidth - getPaddingRight();
                left = right - viewWidth;
            }
        }

        float vMul = calculateVerticalDisplacementForItem(rowIndex, columnsCount, columnIndex);
        top = (int) (rowIndex * viewHeight * vMul);
        bottom = top + viewHeight;
        measureChildWithMargins(view, 0, 0);


        // We calculate everything with View's bounding box (which includes decor and margins)
        // To calculate correct layout position, we subtract margins.
        layoutDecorated(view,
                left + params.leftMargin,
                top + params.topMargin,
                right - params.rightMargin,
                bottom - params.bottomMargin);
        // Consume the available space if the view is not removed OR changed
        if (params.isItemRemoved() || params.isItemChanged()) {
            result.mIgnoreConsumed = true;
        }
        result.mFocusable = view.isFocusable();
    }

    private float calculateVerticalDisplacementForItem(int rowIndex, int columnsCount, int columnIndex) {
        if ((columnsCount & 0x1) == 0) {
            return calculateVerticalDisplacementForEvenCount(rowIndex, columnsCount, columnIndex);
        } else {
            return calculateVerticalDisplacementForNonEvenCount(rowIndex, columnsCount, columnIndex);
        }
    }

    private float calculateVerticalDisplacementForEvenCount(int rowIndex, int columnsCount, int columnIndex) {
        float displace;
        float k;
        float sideDisplacement;
        int centerIndex;
        if (rowIndex > 2) {
            return 1.05f - 0.008f * rowIndex;
        } else if (rowIndex == 1) {
            centerIndex = columnsCount / 2;
            sideDisplacement = 0.88f;
            displace = 0.1f;
            k = 0.05f;
        } else {
            centerIndex = columnsCount / 2;
            sideDisplacement = 0.97f;
            displace = 0.06f;
            k = 0.03f;
        }
        if (columnIndex == 0 || columnIndex == columnsCount - 1) {
            return sideDisplacement;
        } else if (columnIndex < centerIndex - 1) {
            return 1f + displace * columnIndex - k * columnIndex;
        } else if (columnIndex > centerIndex) {
            return 1f + displace * (columnsCount - columnIndex - 1) - k * (columnsCount - columnIndex - 1);
        } else {
            return 1f + displace * centerIndex - k * centerIndex;
        }
    }

    private float calculateVerticalDisplacementForNonEvenCount(int rowIndex, int columnsCount, int columnIndex) {
        float displace;
        float k;
        float sideDisplacement;
        int centerIndex;
        if (rowIndex > 2) {
            return 1.045f - 0.008f * rowIndex;
        } else if (rowIndex == 1) {
            centerIndex = columnsCount / 2;
            sideDisplacement = 0.9f;
            displace = 0.1f;
            k = 0.05f;
        } else {
            centerIndex = columnsCount / 2;
            sideDisplacement = 0.97f;
            displace = 0.06f;
            k = 0.03f;
        }
        if (columnIndex == 0 || columnIndex == columnsCount - 1) {
            return sideDisplacement;
        } else if (columnIndex < centerIndex) {
            return 1f + displace * columnIndex - k * columnIndex;
        } else if (columnIndex > centerIndex) {
            return 1f + displace * (columnsCount - columnIndex - 1) - k * (columnsCount - columnIndex - 1);
        } else {
            return 1f + displace * centerIndex - k * centerIndex;
        }
    }

    @Override
    public boolean canScrollHorizontally() {
        return false;
    }

    @Override
    public boolean canScrollVertically() {
        return false;
    }
}
