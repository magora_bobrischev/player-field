package com.mgrmoby.ontheteam;

import android.app.Application;
import com.facebook.drawee.backends.pipeline.Fresco;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
    }
}
