package com.mgrmoby.ontheteam.drawings;

import android.graphics.*;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public class PlayerFieldDrawable extends Drawable {
    private final Paint linesPaint;
    private final Path linesPath;
    private final int backgroundColor;
    private final int padding;
    private final Rect bounds;

    private PlayerFieldDrawable(@ColorInt int backgroundColor, @ColorInt int linesColor, int linesWidth, int padding) {
        this.backgroundColor = backgroundColor;
        this.padding = padding;
        bounds = new Rect();

        linesPaint = new Paint();
        linesPath = new Path();

        linesPaint.setStyle(Paint.Style.STROKE);
        linesPaint.setStrokeWidth(linesWidth);
        linesPaint.setColor(linesColor);
    }

    @Override
    protected void onBoundsChange(Rect bounds) {
        this.bounds.set(bounds);
        linesPath.reset();
        createOuterBorder(bounds);
        createBottomArcs(bounds);
        createGates(bounds);
        createCenterLine(bounds);
    }

    private void createOuterBorder(@NonNull Rect bounds) {
        int left = bounds.left + padding;
        int right = bounds.right - padding;
        int bottom = bounds.bottom - padding;
        linesPath.moveTo(left, bounds.top);
        linesPath.lineTo(left, bottom);
        linesPath.lineTo(right, bottom);
        linesPath.lineTo(right, bounds.top);
    }

    private void createBottomArcs(Rect bounds) {
        linesPath.addArc(
                0,
                bounds.bottom - padding - padding,
                padding + padding,
                bounds.bottom,
                270,
                90
        );

        linesPath.addArc(
                bounds.right - padding - padding,
                bounds.bottom - padding - padding,
                bounds.right,
                bounds.bottom,
                270,
                -90
        );
    }

    private void createCenterLine(Rect bounds) {
        int lineTopPos = bounds.top + bounds.height() / 8;
        linesPath.moveTo(padding, lineTopPos);
        linesPath.lineTo(bounds.right - padding, lineTopPos);

        int width = Math.min(bounds.width(), bounds.height());
        int centerCircleRadius = width / 7;

        linesPath.addCircle(
                bounds.centerX(),
                lineTopPos,
                centerCircleRadius,
                Path.Direction.CW
        );

        linesPath.addCircle(
                bounds.centerX(),
                lineTopPos,
                6,
                Path.Direction.CW
        );
    }

    private void createGates(Rect bounds) {
        int width = Math.min(bounds.width(), bounds.height());
        int smallGatesWidth = width / 4;
        int smallGatesHeight = smallGatesWidth / 3;
        linesPath.moveTo(bounds.centerX() - smallGatesWidth / 2, bounds.bottom - padding);
        linesPath.rLineTo(0, -smallGatesHeight);
        linesPath.rLineTo(smallGatesWidth, 0);
        linesPath.rLineTo(0, smallGatesHeight);

        int gatesWidth = width / 2;
        int gatesHeight = gatesWidth / 2;
        linesPath.moveTo(bounds.centerX() - gatesWidth / 2, bounds.bottom - padding);
        linesPath.rLineTo(0, -gatesHeight);
        linesPath.rLineTo(gatesWidth, 0);
        linesPath.rLineTo(0, gatesHeight);
        int gatesArcRadius = width / 10;
        linesPath.addArc(
                bounds.centerX() - gatesArcRadius,
                bounds.bottom - padding - gatesHeight - gatesArcRadius,
                bounds.centerX() + gatesArcRadius,
                bounds.bottom - gatesHeight + gatesArcRadius - padding,
                180,
                180);
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        canvas.drawColor(backgroundColor);
        canvas.drawPath(linesPath, linesPaint);
    }

    @Override
    public void setAlpha(int alpha) {
        linesPaint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(ColorFilter colorFilter) {
        linesPaint.setColorFilter(colorFilter);
    }

    @Override
    public int getOpacity() {
        return PixelFormat.OPAQUE;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {

        private Builder() {

        }

        public BackgroundColorBuilder backgroundColor(@ColorInt int color) {
            return new BackgroundColorBuilder(color);
        }
    }

    public static final class BackgroundColorBuilder {
        @ColorInt private final int backgroundColor;

        private BackgroundColorBuilder(@ColorInt int backgroundColor) {
            this.backgroundColor = backgroundColor;
        }

        public LinesColorBuilder linesColor(@ColorInt int color) {
            return new LinesColorBuilder(backgroundColor, color);
        }
    }

    public static final class LinesColorBuilder {
        @ColorInt private final int backgroundColor;
        @ColorInt private final int linesColor;

        private LinesColorBuilder(int backgroundColor, int linesColor) {
            this.backgroundColor = backgroundColor;
            this.linesColor = linesColor;
        }

        public LinesWidthBuilder linesWidth(int width) {
            return new LinesWidthBuilder(backgroundColor, linesColor, width);
        }
    }

    public static final class LinesWidthBuilder {
        @ColorInt private final int backgroundColor;
        @ColorInt private final int linesColor;
        private final int linesWidth;

        private LinesWidthBuilder(int backgroundColor, int linesColor, int linesWidth) {
            this.backgroundColor = backgroundColor;
            this.linesColor = linesColor;
            this.linesWidth = linesWidth;
        }

        public PaddingSizeBuilder padding(int padding) {
            return new PaddingSizeBuilder(backgroundColor, linesColor, linesWidth, padding);
        }
    }

    public static final class PaddingSizeBuilder {
        @ColorInt private final int backgroundColor;
        @ColorInt private final int linesColor;
        private final int linesWidth;
        private final int contentPadding;

        private PaddingSizeBuilder(int backgroundColor, int linesColor, int linesWidth, int contentPadding) {
            this.backgroundColor = backgroundColor;
            this.linesColor = linesColor;
            this.linesWidth = linesWidth;
            this.contentPadding = contentPadding;
        }

        public PlayerFieldDrawable build() {
            return new PlayerFieldDrawable(backgroundColor, linesColor, linesWidth, contentPadding);
        }
    }
}
