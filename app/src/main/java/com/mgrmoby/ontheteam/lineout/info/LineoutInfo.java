package com.mgrmoby.ontheteam.lineout.info;

import android.support.annotation.Nullable;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public interface LineoutInfo {

    @Nullable
    PositionInfo playerInfoForPosition(int position);

    int columnsInRow(int rowNumber);

    int getRowsCount();

    int rowIndexForPosition(int position);

    int columnIndexForPosition(int rowHint, int position);

    int playersCount();

    int maxColumnsInRowCount();
}
