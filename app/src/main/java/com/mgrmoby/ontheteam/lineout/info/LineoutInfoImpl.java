package com.mgrmoby.ontheteam.lineout.info;

import android.support.annotation.NonNull;
import android.util.SparseArray;

import java.util.List;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public class LineoutInfoImpl implements LineoutInfo {
    public final SparseArray<List<PositionInfo>> players;
    private final int playersCount;
    private final int maxColumnsInRowCount;

    public LineoutInfoImpl(SparseArray<List<PositionInfo>> players) {
        this.players = players;
        playersCount = calculatePlayersCount(players);
        maxColumnsInRowCount = findMaxColumnsCount(players);
    }

    private int calculatePlayersCount(SparseArray<List<PositionInfo>> p) {
        int result = 0;
        for (int i = 0; i < p.size(); ++i) {
            result += p.valueAt(i).size();
        }
        return result;
    }

    private int findMaxColumnsCount(SparseArray<List<PositionInfo>> p) {
        int max = 0;
        for (int i = 0; i < p.size(); ++i) {
            max = Math.max(max, p.valueAt(i).size());
        }
        return max;
    }

    @Override
    public int columnsInRow(int rowNumber) {
        return players.get(rowNumber).size();
    }

    @Override
    public int getRowsCount() {
        return players.size();
    }

    @Override
    public int rowIndexForPosition(int position) {
        int checkedCount = 0;
        for (int i = 0; i < players.size(); ++i) {
            checkedCount += players.get(i).size();
            if (position < checkedCount) {
                return i;
            }
        }
        //Should never happens
        return 0;
    }

    @Override
    public int columnIndexForPosition(int rowHint, int position) {
        if (rowHint == -1) {
            rowHint = rowIndexForPosition(position);
        }
        for (int i = 0; i < rowHint; ++i) {
            position -= players.get(i).size();
        }
        return position;
    }

    @Override
    public int maxColumnsInRowCount() {
        return maxColumnsInRowCount;
    }

    @NonNull
    @Override
    public PositionInfo playerInfoForPosition(int position) {
        int row = rowIndexForPosition(position);
        int column = columnIndexForPosition(row, position);
        return players.get(row).get(column);

    }

    @Override
    public int playersCount() {
        return playersCount;
    }
}
