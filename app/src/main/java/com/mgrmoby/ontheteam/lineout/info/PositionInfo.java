package com.mgrmoby.ontheteam.lineout.info;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.mgrmoby.ontheteam.lineout.player.Player;
import com.mgrmoby.ontheteam.lineout.player.Position;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public class PositionInfo {
    private Player player;
    private Position position = Position.UNKNOWN;

    private PositionInfo() {
    }

    private PositionInfo(@Nullable Player player, @NonNull Position position) {
        this.player = player;
        this.position = position;
    }

    @Nullable
    public Player player() {
        return player;
    }

    @NonNull
    public Position position() {
        return position == null ? Position.UNKNOWN : position;
    }

    public boolean isEmpty() {
        return player == null;
    }

    public static PositionInfo of(@NonNull Position position) {
        return new PositionInfo(null, position);
    }

    public static PositionInfo of(Player player, @NonNull Position position) {
        return new PositionInfo(player, position);
    }

    @Override
    public String toString() {
        return "PositionInfo{" +
                "player=" + (player == null ? "EMPTY" : player) +
                ", position=" + position +
                '}';
    }
}
