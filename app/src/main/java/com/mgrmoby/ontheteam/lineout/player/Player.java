package com.mgrmoby.ontheteam.lineout.player;

import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public class Player {
    @Nullable public final String name;
    @DrawableRes public final int avatar;

    public Player(@Nullable String name, @DrawableRes int avatar) {
        this.name = name;
        this.avatar = avatar;
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", avatar=" + avatar +
                '}';
    }
}
