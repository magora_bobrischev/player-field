package com.mgrmoby.ontheteam.lineout.player;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public enum  Position {
    UNKNOWN(""),
    FORWARD("F"),
    HALFBACK("H"),
    DEFENDER("D"),
    GOALKEEPER("G");

    public final String position;

    Position(String position) {
        this.position = position;
    }
}
