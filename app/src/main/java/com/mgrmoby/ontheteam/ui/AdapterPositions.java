package com.mgrmoby.ontheteam.ui;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Size;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import com.mgrmoby.ontheteam.lineout.info.LineoutInfo;
import com.mgrmoby.ontheteam.lineout.info.PositionInfo;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public class AdapterPositions extends RecyclerView.Adapter<PositionInfoHolder> {
    private final OnPositionClickListener onPlayerClickListener;
    private LineoutInfo lineoutInfo;
    private Size avatarSize;

    public interface OnPositionClickListener {

        void onPlayerClicked(@NonNull View v, @Nullable PositionInfo positionInfo);
    }

    public AdapterPositions(@NonNull LineoutInfo params, @NonNull OnPositionClickListener onPlayerClickListener) {
        this.lineoutInfo = params;
        this.onPlayerClickListener = onPlayerClickListener;
    }

    @Override
    public PositionInfoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PositionInfoHolder holder = PositionInfoHolder.createHolder(parent);
        if (avatarSize == null) {
            avatarSize = calculateAvatarMaxSize(parent, holder.itemView);
        }
        holder.imagePositionPicture.adjustLayoutParams(avatarSize.getHeight(), false);

        holder.imagePositionPicture.setOnClickListener(v -> {
            int adapterPosition = holder.getAdapterPosition();
            if (adapterPosition != RecyclerView.NO_POSITION) {
                onPlayerClickListener.onPlayerClicked(holder.imagePositionPicture, lineoutInfo.playerInfoForPosition(adapterPosition));
            }
        });
        return holder;
    }

    private Size calculateAvatarMaxSize(ViewGroup parent, View holderItemView) {
        int parentWidth = parent.getMeasuredWidth() - parent.getPaddingStart() - parent.getPaddingEnd();
        int parentHeight = parent.getMeasuredHeight() - parent.getPaddingTop() - parent.getPaddingBottom();
        int maxCol = lineoutInfo.maxColumnsInRowCount();
        int rows = lineoutInfo.getRowsCount();
        int maxWidth = parentWidth / maxCol - holderItemView.getPaddingStart() - holderItemView.getPaddingEnd();
        int maxHeight = parentHeight / rows - holderItemView.getPaddingTop() - holderItemView.getPaddingBottom() -
                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 28 /*TODO: WTF -> NO WTF*/, parent.getResources().getDisplayMetrics());
        int size = Math.min(maxWidth, maxHeight);
        return new Size(size, size);
    }

    @Override
    public void onBindViewHolder(PositionInfoHolder holder, int position) {
        holder.bind(lineoutInfo.playerInfoForPosition(position));
    }

    @Override
    public int getItemCount() {
        return lineoutInfo.playersCount();
    }
}
