package com.mgrmoby.ontheteam.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import com.example.grey.playerfieldview.R;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public class AvatarImageView extends PorterShapeImageView {
    private static final float DEFAULT_RATIO = 1f;
    private float aspectRatio = DEFAULT_RATIO;

    public AvatarImageView(Context context) {
        this(context, null);
    }

    public AvatarImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AvatarImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initWithAttrs(context, attrs, defStyle);
    }

    private void initWithAttrs(Context context, AttributeSet attrs, int defStyle) {
        if (attrs != null) {
            TypedArray array = context.getTheme().obtainStyledAttributes(attrs, R.styleable.AvatarImageView, defStyle, 0);
            aspectRatio = array.getFloat(R.styleable.AvatarImageView_siAspectRatio, DEFAULT_RATIO);
            array.recycle();
        }
    }

    public void adjustLayoutParams(int height, boolean requestLayout) {
        getLayoutParams().width = Math.round(height * aspectRatio);
        getLayoutParams().height = height;
        if (requestLayout) {
            requestLayout();
        }
    }
}
