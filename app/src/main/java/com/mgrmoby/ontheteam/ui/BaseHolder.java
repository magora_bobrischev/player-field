package com.mgrmoby.ontheteam.ui;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public abstract class BaseHolder<T> extends RecyclerView.ViewHolder {

    protected BaseHolder(@NonNull View itemView) {
        super(itemView);
    }

    public abstract void bind(@Nullable T item);
}
