package com.mgrmoby.ontheteam.ui;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LineoutLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.example.grey.playerfieldview.R;
import com.mgrmoby.ontheteam.drawings.PlayerFieldDrawable;
import com.mgrmoby.ontheteam.lineout.info.LineoutInfo;
import com.mgrmoby.ontheteam.lineout.info.LineoutInfoImpl;
import com.mgrmoby.ontheteam.lineout.info.PositionInfo;
import com.mgrmoby.ontheteam.lineout.player.Player;
import com.mgrmoby.ontheteam.lineout.player.Position;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;

    private enum Formation {
        F_2_3_3_2() {
            @Override
            public LineoutInfo generate() {
                SparseArray<List<PositionInfo>> players = new SparseArray<>();
                players.put(0, Arrays.asList(
                        PositionInfo.of(new Player("Zidan", R.drawable.ic_avatar), Position.FORWARD),
                        PositionInfo.of(Position.FORWARD)));

                players.put(1, Arrays.asList(
                        PositionInfo.of(Position.HALFBACK),
                        PositionInfo.of(new Player("Messi", R.drawable.ic_avatar), Position.HALFBACK),
                        PositionInfo.of(Position.HALFBACK)));

                players.put(2, Arrays.asList(
                        PositionInfo.of(new Player("Dzhigurda", R.drawable.ic_avatar), Position.DEFENDER),
                        PositionInfo.of(new Player("Ronaldinho", R.drawable.ic_avatar), Position.DEFENDER),
                        PositionInfo.of(Position.DEFENDER)));

                players.put(3, Arrays.asList(
                        PositionInfo.of(Position.DEFENDER),
                        PositionInfo.of(Position.DEFENDER)));

                players.put(4, Arrays.asList(PositionInfo.of(Position.GOALKEEPER)));

                return new LineoutInfoImpl(players);
            }

            @Override
            public Formation next() {
                return F_1_3_4_2;
            }

            @Override
            public String simplyName() {
                return "2-3-3-2";
            }
        },
        F_1_3_4_2() {
            @Override
            public LineoutInfo generate() {
                SparseArray<List<PositionInfo>> players = new SparseArray<>();
                players.put(0, Arrays.asList(PositionInfo.of(Position.FORWARD)));

                players.put(1, Arrays.asList(
                        PositionInfo.of(Position.HALFBACK),
                        PositionInfo.of(Position.HALFBACK),
                        PositionInfo.of(Position.HALFBACK)));

                players.put(2, Arrays.asList(
                        PositionInfo.of(Position.DEFENDER),
                        PositionInfo.of(Position.DEFENDER),
                        PositionInfo.of(Position.DEFENDER),
                        PositionInfo.of(Position.DEFENDER)));

                players.put(3, Arrays.asList(
                        PositionInfo.of(Position.DEFENDER),
                        PositionInfo.of(Position.DEFENDER)));

                players.put(4, Arrays.asList(PositionInfo.of(Position.GOALKEEPER)));

                return new LineoutInfoImpl(players);
            }

            @Override
            public Formation next() {
                return F_3_3_2_2;
            }

            @Override
            public String simplyName() {
                return "1-3-4-2";
            }
        },
        F_3_3_2_2() {
            @Override
            public LineoutInfo generate() {
                SparseArray<List<PositionInfo>> players = new SparseArray<>();
                players.put(0, Arrays.asList(
                        PositionInfo.of(Position.FORWARD),
                        PositionInfo.of(Position.FORWARD),
                        PositionInfo.of(Position.FORWARD)));

                players.put(1, Arrays.asList(
                        PositionInfo.of(Position.HALFBACK),
                        PositionInfo.of(Position.HALFBACK),
                        PositionInfo.of(Position.HALFBACK)));

                players.put(2, Arrays.asList(
                        PositionInfo.of(Position.DEFENDER),
                        PositionInfo.of(Position.DEFENDER)));

                players.put(3, Arrays.asList(
                        PositionInfo.of(Position.DEFENDER),
                        PositionInfo.of(Position.DEFENDER)));

                players.put(4, Arrays.asList(PositionInfo.of(Position.GOALKEEPER)));

                return new LineoutInfoImpl(players);
            }

            @Override
            public Formation next() {
                return F_2_5_3;
            }

            @Override
            public String simplyName() {
                return "3-3-2-2";
            }
        },
        F_2_5_3() {
            @Override
            public LineoutInfo generate() {
                SparseArray<List<PositionInfo>> players = new SparseArray<>();
                players.put(0, Arrays.asList(
                        PositionInfo.of(Position.FORWARD),
                        PositionInfo.of(Position.FORWARD)));

                players.put(1, Arrays.asList(
                        PositionInfo.of(Position.HALFBACK),
                        PositionInfo.of(Position.HALFBACK),
                        PositionInfo.of(Position.HALFBACK),
                        PositionInfo.of(Position.HALFBACK),
                        PositionInfo.of(Position.HALFBACK)));

                players.put(2, Arrays.asList(
                        PositionInfo.of(Position.DEFENDER),
                        PositionInfo.of(Position.DEFENDER),
                        PositionInfo.of(Position.DEFENDER)));

                players.put(3, Arrays.asList(PositionInfo.of(Position.GOALKEEPER)));

                return new LineoutInfoImpl(players);
            }

            @Override
            public Formation next() {
                return F_2_3_3_2;
            }

            @Override
            public String simplyName() {
                return "2-5-3";
            }
        };

        public abstract LineoutInfo generate();

        public abstract Formation next();

        public abstract String simplyName();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        LineoutInfo params = Formation.F_2_5_3.generate();
        final LineoutLayoutManager manager = new LineoutLayoutManager(this, params, false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(new AdapterPositions(params, (v, player) -> {
            Toast.makeText(MainActivity.this, "Click: " + player, Toast.LENGTH_SHORT).show();
        }));
        Drawable playerFieldBackground = PlayerFieldDrawable.builder()
                .backgroundColor(ContextCompat.getColor(this, R.color.color_field_background))
                .linesColor(ContextCompat.getColor(this, R.color.color_field_lines))
                .linesWidth(4)
                .padding((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics()))
                .build();
        recyclerView.setBackground(playerFieldBackground);

        findViewById(R.id.button_formation).setOnClickListener(new View.OnClickListener() {
            private Formation current = Formation.F_2_5_3;

            @Override
            public void onClick(View view) {
                current = current.next();
                LineoutInfo info = current.generate();
                manager.swapInfo(info);
                recyclerView.setAdapter(new AdapterPositions(info, (v, player) -> {
                    Toast.makeText(MainActivity.this, "Click: " + player, Toast.LENGTH_SHORT).show();
                }));

                ((Button) view).setText(current.simplyName());
            }
        });
    }
}
