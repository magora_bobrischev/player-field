package com.mgrmoby.ontheteam.ui;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatDrawableManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.grey.playerfieldview.R;
import com.mgrmoby.ontheteam.lineout.info.PositionInfo;
import com.mgrmoby.ontheteam.lineout.player.Player;
import com.mgrmoby.ontheteam.utils.MeasureHelper;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
@SuppressWarnings({"WeakerAccess", "ConstantConditions"})
public class PositionInfoHolder extends BaseHolder<PositionInfo> {
    public AvatarImageView imagePositionPicture;
    public TextView textLabel;
    private Drawable overlayDrawable;

    private PositionInfoHolder(@NonNull View itemView) {
        super(itemView);
        imagePositionPicture = (AvatarImageView) itemView.findViewById(R.id.id_player_avatar);
        textLabel = (TextView) itemView.findViewById(R.id.text_name);
    }

    @Override
    public void bind(@Nullable PositionInfo info) {
        if (info == null) {
            return;
        }
        if (!info.isEmpty()) {
            Player player = info.player();
            if (player.avatar != 0) {
                imagePositionPicture.setImageResource(player.avatar);
                textLabel.setText(player.name);
                showSwapOverlay();
            } else {
                imagePositionPicture.setImageResource(-1);
                textLabel.setText(info.position().position);
            }
        } else {
            textLabel.setText(info.position().position);
        }
    }

    private void showSwapOverlay() {
        MeasureHelper.waitForMeasure(imagePositionPicture, () -> {
            if (overlayDrawable == null) {
                overlayDrawable = generateOverlayDrawable(imagePositionPicture);
            }
            itemView.getOverlay().add(overlayDrawable);
        });
    }

    private Drawable generateOverlayDrawable(@NonNull View toPositionOn) {
        Drawable d = AppCompatDrawableManager.get().getDrawable(itemView.getContext(), R.drawable.ic_swap);
        d.setBounds(
                toPositionOn.getRight() - d.getIntrinsicWidth() / 2,
                toPositionOn.getTop() - d.getIntrinsicHeight() / 2,
                toPositionOn.getRight() + d.getIntrinsicWidth() / 2,
                toPositionOn.getTop() + d.getIntrinsicHeight() / 2
        );
        return d;
    }

    public static PositionInfoHolder createHolder(ViewGroup parent) {
        return new PositionInfoHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.holder_player, parent, false));
    }
}
