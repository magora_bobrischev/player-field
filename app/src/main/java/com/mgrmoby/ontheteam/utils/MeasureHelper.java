package com.mgrmoby.ontheteam.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewTreeObserver;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public class MeasureHelper {
    private MeasureHelper() {

    }

    public static void waitForMeasure(@Nullable View v, @NonNull Runnable onMeasure) {
        if (v != null) {
            if (v.getMeasuredWidth() != 0 && v.getMeasuredHeight() != 0) {
                onMeasure.run();
            } else {
                v.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        v.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        onMeasure.run();
                    }
                });
            }
        }
    }
}
